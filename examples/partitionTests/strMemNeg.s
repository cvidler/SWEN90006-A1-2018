;;testing simple
MOV R1 65535;
MOV R2 100
MOV R3 -65535;
;ADD R4 R2 R3; R4 = 100 + -65535
;ADD R4 R4 R1; R4 = -65435 + 65535
;SUB R5 R4 R1; R5 = 100 - 65535
;SUB R5 R5 R3; R5 = -65435 - - 65535

;;testing simple
MOV R1 65535;
MOV R2 100
MOV R3 -65535;
ADD R4 R2 R3;
SUB R5 R4 R1;
MUL R6 R4 R5
DIV R7 R6 R5;  R7 = 152




;;testing STR and LDR
MOV R10 -1;
STR R10 0 R10; ;;does nothing as b + v < -1
LDR R12 0 R10;  R12 = 0

ADD R13 R12 R7; R13 = 152


;;testing loop jz and jmp
MOV R14 10;
MOV R15 1;
MOV R18 1;
MOV R17 1;
SUB R16 R14 R15
JZ  R16 4
ADD R17 R17 R16
ADD R15 R15 R18
JMP -4                  ;R17 = 46

ADD R19 R13 R17         ; 172 + 46
RET R19                  ; return sum;
