package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //EC3 , EC6 EC15, EC18, EC19 all tested in one
  @Test public void Valid()
  {
    final List<String> lines = readInstructions("examples/Valid.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -2147303305);
  }



  //EC1 - Ri, i is negative
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
   public void RiNeg()
  {
    final List<String> lines = readInstructions("examples/RiNeg.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  //EC2 - Register value too high
  @Test public void RiValueHigh()
  {
    final List<String> lines = readInstructions("examples/RiValueHigh.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -2147483648);
  }

  //EC4 - Ri, i too high > 31
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
   public void RiHigh()
  {
    final List<String> lines = readInstructions("examples/RiHigh.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  //EC5 - Value too low <-65535
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
   public void valueLow()
  {
    final List<String> lines = readInstructions("examples/valueLow.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  //EC7 - Value too high >65535
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
   public void valueHigh()
  {
    final List<String> lines = readInstructions("examples/valueHigh.s");
    Machine m = new Machine();
    m.execute(lines);
  }
    //EC9 -- jumps to PC = 1 as PC = 0 caused infinite loop
    @Test public void JmpFirst()
    {
      final List<String> lines = readInstructions("examples/jmpFirst.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 2);
    }

    //EC8 - Jumps to negative pc, value=-c
    @Test(expected = swen90006.machine.NoReturnValueException.class)
     public void JmpNeg()
    {
      final List<String> lines = readInstructions("examples/jmpNeg.s");
      Machine m = new Machine();
      m.execute(lines);
    }
    //EC9 - jumps to PC = N
    @Test public void JmpLast()
    {
      final List<String> lines = readInstructions("examples/jmpLast.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 100);
    }
    //EC10 - Jumps past end of PC>N , value > N -c
    @Test(expected = swen90006.machine.NoReturnValueException.class)
     public void JmpPast()
    {
      final List<String> lines = readInstructions("examples/jmpPast.s");
      Machine m = new Machine();
      m.execute(lines);
    }


  //EC12 - JZ to  PC= 1 as pc = 0 causes infinite loop
  @Test public void JzFirst()
  {
    final List<String> lines = readInstructions("examples/jzfirst.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -1);
  }

  //EC11 - jz jumps to negative pc=-1, value = -c
  @Test(expected = swen90006.machine.NoReturnValueException.class)
   public void JzNeg()
  {
    final List<String> lines = readInstructions("examples/jzNeg.s");
    Machine m = new Machine();
    m.execute(lines);
  }
    //EC12 - JZ to  PC= N
  @Test public void JzLast()
  {
    final List<String> lines = readInstructions("examples/jzLast.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 100);
  }
  //EC13 - jz jumps past end of PC = N+1, value = N-c+ 1
  @Test(expected = swen90006.machine.NoReturnValueException.class)
   public void JzPast()
  {
    final List<String> lines = readInstructions("examples/jzPast.s");
    Machine m = new Machine();
    m.execute(lines);
  }



  //EC14 - string store and load memory location negative, memory[-1]
  @Test public void StrNeg()
  {
    final List<String> lines = readInstructions("examples/strNeg.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }


  // //EC15 -- Memory[0] -- implemented in valid test
  // @Test public void StrZero()
  // {
  //   final List<String> lines = readInstructions("examples/strZero.s");
  //   Machine m = new Machine();
  //   assertEquals(m.execute(lines), 12);
  // }
  // //EC15 -- Memory[65535] -- implmeneted in valid test
  // @Test public void StrMax()
  // {
  //   final List<String> lines = readInstructions("examples/strMax.s");
  //   Machine m = new Machine();
  //   assertEquals(m.execute(lines), 12);
  // }


  //EC16 - String store and load mormory location > MAX , memory[65536]
  @Test public void StrHigh()
  {
    final List<String> lines = readInstructions("examples/strHigh.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }

  //EC18 - Divide by 0
  @Test public void Div()
  {
    final List<String> lines = readInstructions("examples/div.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 100);
  }


  //EC17 - No return reachable
  @Test(expected = swen90006.machine.NoReturnValueException.class)
   public void NoReturn()
  {
    final List<String> lines = readInstructions("examples/NoReturn.s");
    Machine m = new Machine();
    m.execute(lines);
  }



  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
