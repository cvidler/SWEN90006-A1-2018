package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

    //the assertTrue method is used to check whether something holds.
    //assertTrue(list.contains("a"));



//Assignment tests cases from leaf nodes in test template tree:

  //EC3 , EC6 EC15, EC18 all tested in one
  @Test public void Valid()
  {
    final List<String> lines = readInstructions("examples/partitionTests/validfile.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 218);
  }



//EC1 - Ri, i is negative
@Test(expected = swen90006.machine.InvalidInstructionException.class)
public void RiNeg()
{
  final List<String> lines = readInstructions("examples/partitionTests/RiNeg.s");
  Machine m = new Machine();
  m.execute(lines);
}
  //EC4 - Ri, i too high > 31
@Test(expected = swen90006.machine.InvalidInstructionException.class)
public void RiHigh()
{
  final List<String> lines = readInstructions("examples/partitionTests/RiHigh.s");
  Machine m = new Machine();
  m.execute(lines);
}

  //EC7 - Value too high >65535
@Test(expected = swen90006.machine.InvalidInstructionException.class)
public void ValueHigh()
{
  final List<String> lines = readInstructions("examples/partitionTests/valueHigh.s");
  Machine m = new Machine();
  m.execute(lines);
}


  //EC5 - Value too low <-65535
@Test(expected = swen90006.machine.InvalidInstructionException.class)
public void ValueLow()
{
  final List<String> lines = readInstructions("examples/partitionTests/valueLow.s");
  Machine m = new Machine();
  m.execute(lines);
}

  //EC2 - Register value too high, register value = maxvalue + 1
@Test public void RiMaxValue()
{
  final List<String> lines = readInstructions("examples/partitionTests/RiMaxValue.s");
  Machine m = new Machine();
  assertEquals(m.execute(lines), -2147483648);
}

  //EC8 - Jumps to negative pc, value=-c
@Test(expected = swen90006.machine.NoReturnValueException.class)
public void JmpNeg()
{
  final List<String> lines = readInstructions("examples/partitionTests/jmpNeg.s");
  Machine m = new Machine();
  m.execute(lines);
}

    //EC10 - Jumps past end of PC>N , value > N -c
@Test(expected = swen90006.machine.NoReturnValueException.class)
public void JmpHigh()
{
  final List<String> lines = readInstructions("examples/partitionTests/jmpHigh.s");
  Machine m = new Machine();
  m.execute(lines);
}

  //EC11 - jz jumps to negative pc=-1, value = -c
@Test(expected = swen90006.machine.NoReturnValueException.class)
public void jzNeg()
{
  final List<String> lines = readInstructions("examples/partitionTests/jzNeg.s");
  Machine m = new Machine();
  m.execute(lines);
}

  //EC13 - jz jumps past end of PC = N+1, value = N-c+ 1
@Test(expected = swen90006.machine.NoReturnValueException.class)
public void jzHigh()
{
  final List<String> lines = readInstructions("examples/partitionTests/jzHigh.s");
  Machine m = new Machine();
  m.execute(lines);
}

//EC16 - String store and load mormory location > MAX , memory[65536]

@Test public void strHigh()
{
  final List<String> lines = readInstructions("examples/partitionTests/strMaxMem.s");
  Machine m = new Machine();
  assertEquals(m.execute(lines), 0);
}
  //EC14 - string store and load memory location negative, memory[-1]

@Test(expected = swen90006.machine.InvalidInstructionException.class)
public void strNeg()
{
  final List<String> lines = readInstructions("examples/partitionTests/strMemNeg.s");
  Machine m = new Machine();
  assertEquals(m.execute(lines), 0);
}







// //token is value instead of register
// @Test(expected = swen90006.machine.InvalidInstructionException.class)
// public void RChange()
// {
//   final List<String> lines = readInstructions("examples/RChange.s");
//   Machine m = new Machine();
//   assertEquals(m.execute(lines), -64535);
// }

//


///potential test for register value too high


//EC17- No return reachable
@Test(expected = swen90006.machine.NoReturnValueException.class)
public void NoReturn()
{
  final List<String> lines = readInstructions("examples/partitionTests/noReturn.s");
  Machine m = new Machine();
  m.execute(lines);
}

//EC18 - Divide by 0
@Test public void Div()
{
  final List<String> lines = readInstructions("examples/div.s");
  Machine m = new Machine();
  assertEquals(m.execute(lines), 100);
}



  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
